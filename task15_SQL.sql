use labor_sql;

#task1_19
-- select * from classes
-- where country='Japan'
-- order by type;

#task1_20
-- select name, launched from ships
-- where launched between 1920 and 1942
-- order by launched;

#task1_21
-- select * from outcomes
-- where battle = 'Guadalcanal' and result != 'sunk'
-- order by ship desc;

#task1_22
-- select * from outcomes
-- where result = 'sunk'
-- order by ship desc;

#task1_23
-- select class, displacement from classes
-- where displacement > 40000
-- order by type;

#task2_4
-- select name from ships
-- where name like 'W%on'

#task2_5 - wrong!!!
-- select name from ships
-- where name rlike '*e{2}';

#task2_6
-- select name, launched from ships
-- where name not like '%e'

#task2_7
-- select * from battles
-- where name like '% %c';

#task3_4 - wrong!!!
-- select l1.model model1,l2.model model2, l1.ram, l1.hd
-- from laptop l1, laptop l2
-- where (l1.ram=l2.ram) and (l1.hd = l2.hd);


#task3_5
-- select distinct c2.country, c1.type type1,c2.type type2
-- from classes c1,classes c2
-- where c1.type = 'bb' and c2.type = 'bc';

#task3_11
-- select distinct s.name, c.displacement from ships s
-- join classes c on s.class = c.class

#task3_12
-- select o.ship, b.name battle, b.date
-- from outcomes o
-- join battles b on o.battle = b.name
-- where result !='sunk';

#task3_13
-- select country, name
-- from ships
-- join classes on classes.class = ships.class
-- order by country



